import { MasterBankComponent } from './../master-bank/master-bank.component';
import { MasterPegawaiComponent } from './../master-pegawai/master-pegawai.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';


const appRoutes: Routes = [
  { path: '', redirectTo: '/pegawai', pathMatch: 'full' },
  { path: 'pegawai', component: MasterPegawaiComponent },
  { path: 'bank', component: MasterBankComponent }
  ];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule],
  declarations: []
})
export class RoutingModule { }
