import {HttpClientModule} from '@angular/common/http';
import { PegawaiService } from './pegawai.service';
import {HttpModule, Http} from '@angular/http';
import { RoutingModule } from './router/routing.module';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { MasterPegawaiComponent } from './master-pegawai/master-pegawai.component';
import { MasterBankComponent } from './master-bank/master-bank.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    MasterPegawaiComponent,
    MasterBankComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    RoutingModule,
    HttpModule
  ],
  providers: [PegawaiService],
  bootstrap: [AppComponent]
})
export class AppModule { }
