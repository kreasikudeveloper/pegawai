import { Bank } from './../model/bank.model';
import { NgForm } from '@angular/forms';
import { Component, OnInit, ViewChild } from '@angular/core';

@Component({
  selector: 'app-master-bank',
  templateUrl: './master-bank.component.html',
  styleUrls: ['./master-bank.component.css']
})
export class MasterBankComponent implements OnInit {

  @ViewChild('fb') valueForm: NgForm;
  editMode:boolean;
  banks:Bank[]=[
    new Bank('00773827666', 'Bank Mandiri','Pacung'),
    new Bank('07299111971', 'Bank CIMB Niaga','Tabanan')
  ];

  lastIndex:number=0;

  constructor() { }

  ngOnInit() {
  }

  onAddBank(form: NgForm) {
    const value = form.value;
    console.log(value.txtNama + " - " + value.txtCabang);
    let bank:Bank = new Bank(value.txtAccNo, value.txtNama, value.txtCabang);
    if(this.editMode==true){
        this.updateDataByIndex(this.lastIndex, bank);
    }else{
      this.banks.push(bank);
    }
    this.onClear();
  }

  onClear(){
    this.valueForm.reset();
    this.editMode=false;
    console.log("test");
  }

  updateDataByIndex(index: number, newBank: Bank) {
    this.banks[index] = newBank;
  }

  updateData(ba: Bank, index: number) {
    this.valueForm.setValue({
      txtAccNo: ba.accNo,
      txtNama: ba.nama,
      txtCabang: ba.cabang
    });
    this.editMode=true;
    this.lastIndex = index;
  }


  deleteData(index:number){
    this.banks.splice(index, 1);
  }

}
