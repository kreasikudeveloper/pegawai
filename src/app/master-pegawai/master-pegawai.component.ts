import { PegawaiService } from './../pegawai.service';
import { Pegawai } from './../model/pegawai.model';
import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-master-pegawai',
  templateUrl: './master-pegawai.component.html',
  styleUrls: ['./master-pegawai.component.css']
})
export class MasterPegawaiComponent implements OnInit {

  @ViewChild('f') valueForm: NgForm;
  editMode:boolean;
  pegawais:Pegawai[];

  lastIndex:number=0;

  constructor(private ps:PegawaiService) { 
      // this.pegawais = ps.getDataPegawai();
      ps.getDataPegawai().subscribe(
        data => this.pegawais = data
      );
  }

  ngOnInit() {
  }

  onAddItem(form: NgForm) {
    const value = form.value;
    console.log(value.txtNama + " - " + value.txtAlamat);
    console.log(form);
    let pegawai:Pegawai = new Pegawai(value.txtNik, value.txtNama, value.txtAlamat, value.txtPathFoto);
    if(this.editMode==true){
        this.updateDataByIndex(this.lastIndex, pegawai);
    }else{
      this.pegawais.push(pegawai);
    }
    this.onClear();
  }

  onClear(){
    this.valueForm.reset();
    this.editMode=false;
  }

  updateDataByIndex(index: number, newPegawai: Pegawai) {
    this.pegawais[index] = newPegawai;
  }

  updateData(peg: Pegawai, index: number) {
    this.valueForm.setValue({
      txtNik: peg.nik,
      txtNama: peg.nama,
      txtAlamat: peg.alamat,
      txtPathFoto: peg.pathfoto
    });
    this.editMode=true;
    this.lastIndex = index;
  }


  deleteData(index:number){
    this.pegawais.splice(index, 1);
  }

}
