import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MasterPegawaiComponent } from './master-pegawai.component';

describe('MasterPegawaiComponent', () => {
  let component: MasterPegawaiComponent;
  let fixture: ComponentFixture<MasterPegawaiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MasterPegawaiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MasterPegawaiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
