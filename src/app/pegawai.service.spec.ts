import { TestBed, inject } from '@angular/core/testing';

import { PegawaiService } from './pegawai.service';

describe('PegawaiService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PegawaiService]
    });
  });

  it('should be created', inject([PegawaiService], (service: PegawaiService) => {
    expect(service).toBeTruthy();
  }));
});
