import { Pegawai } from './model/pegawai.model';
import {Http, Response} from '@angular/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';

@Injectable()
export class PegawaiService {

  // pegawais:Pegawai[]=[
  //   new Pegawai('123456', 'Balicamp','Pacung' ,'https://cdn2.iconfinder.com/data/icons/user-management/512/link-512.png'),
  //   new Pegawai('223345', 'Telkomsigma', 'BSD', 'https://cdn2.iconfinder.com/data/icons/user-management/512/link-512.png')
  // ]; 
  pegawais:Pegawai[];

  constructor(private http:Http) { }

  getDataPegawai(){
    return this.http.get('http://localhost:8080/pegawai')
    .map((res: Response) => this.pegawais=res.json());
  }


}


