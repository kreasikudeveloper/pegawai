export class Pegawai {
    public nik:string;
    public nama:string;
    public alamat:string;
    public pathfoto:string;


	constructor($nik: string, $nama: string, $alamat: string, $pathfoto: string) {
		this.nik = $nik;
		this.nama = $nama;
		this.alamat = $alamat;
		this.pathfoto = $pathfoto;
	}
    
}