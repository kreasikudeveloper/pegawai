export class Bank {
    public accNo:string;
    public nama:string;
    public cabang:string;


	constructor($accNo: string, $nama: string, $cabang: string) {
		this.accNo = $accNo;
		this.nama = $nama;
		this.cabang = $cabang;
	}

    
}